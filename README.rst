================
safetydance_test
================

A ``safetydance`` library of steps for testing.


Description
===========

A longer description of your project goes here...


Note
====

This project has been set up using PyScaffold 3.2.1. For details and usage
information on PyScaffold see https://pyscaffold.org/.

Documentation
=============

Documentation for this project is here https://openteams.gitlab.io/safetydance_test/
